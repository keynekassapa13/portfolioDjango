from django.shortcuts import render
from django.http import HttpResponse
from .models import Blog
import xml.etree.ElementTree as ET
import requests

# Create your views here.
def index(request):
    return render(request, 'blogs/index.html')

def blog(request):
	if Medium.get():
		context = {'blogs':Blog.objects.all()}

	return render(request, 'blogs/index.html', context)

class Medium:
	def get():
		Blog.objects.all().delete()
		r = requests.get('https://medium.com/feed/@keynekassapa13')
		xml = ET.fromstring(r.content)
		elements = xml[0][9:]
		for element in elements:
			blog = Blog()
			blog.title = element[0].text
			blog.link = element[1].text
			blog.content = element[-1].text
			blog.datePub = element.find('pubDate').text
			blog.save()
		return '200'
