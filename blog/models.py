from django.db import models

# Create your models here.
class Blog(models.Model):
	"""docstring for Blog"""
	title = models.CharField(max_length=250)
	link = models.TextField()
	content = models.TextField()
	datePub = models.CharField(max_length=250)

	def __str__(self):
		return self.title
		