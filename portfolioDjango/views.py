from django.http import HttpResponse
from django.shortcuts import render
import xml.etree.ElementTree as ET
import requests

def index(request):
    return render(request, 'index.html')

def about(request):
	return render(request, 'about.html')

def home(request):
	return render(request, 'home.html')

def error_404(request):
    data = {}
    return render(request,'404.html', data)

def error_500(request):
    data = {}
    return render(request,'404.html', data)

'''def blog(request):
	title, link = Medium.get()
	return render(request, 'blogs/index.html', {'link': link, 'title': title})

class Medium:
	def get():
		r = requests.get('https://medium.com/feed/@keynekassapa13')
		xml = ET.fromstring(r.content)
		elements = xml[0][9:]
		title = xml[0][9][0].text
		link = xml[0][9][1].text
		return title, link'''


