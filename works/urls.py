from django.contrib import admin
from django.urls import path
from . import views

app_name = 'works'

urlpatterns = [
    path('', views.index, name="workIndex"),
    path('<work_id>/', views.detail, name='workDetail')
]
