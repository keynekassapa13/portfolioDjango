from django.contrib import admin

# Register your models here.
from .models import *
admin.site.register(WorkImage)
admin.site.register(Works)
