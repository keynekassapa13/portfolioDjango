from django.shortcuts import render
from django.views import generic
from .models import Works
import requests
import json

# Create your views here.
def index(request):
	if WorkClass.get():
		context = {'works':Works.objects.all()}

	return render(request, 'works/index.html', context)
	'''template_name = 'works/index.html'

	def get_queryset(self):
		return Works.objects.all().order_by('-pk')'''


def detail(request, work_id):
	url = 'http://www.behance.net/v2/projects/' + work_id + '?api_key=LSLLa7T268qpUueyWqDGJubMvW1xp0W6'
	r = requests.get(url)
	json_data = json.loads(r.text)
	workcat = str(json_data["project"]["fields"])[1:-1].replace("'","")
	workpic = json_data["project"]["modules"][0]["type"]
	if workpic == "image":
		workpic = json_data["project"]["modules"][0]["sizes"]["original"]
	else:
		workpic = json_data["project"]["covers"]["original"]

	context = {
	'workcat':workcat,
	'workname':json_data["project"]["name"],
	'workid':json_data["project"]["id"],
	'workurl':json_data["project"]["url"],
	'workpic':workpic,
	'workdes':json_data["project"]["description"]
	}
	return render(request, 'works/detail.html', context)

class WorkClass:
	def get():
		Works.objects.all().delete()
		r = requests.get('https://api.behance.net/v2/users/keynekassapa13/projects?client_id=LSLLa7T268qpUueyWqDGJubMvW1xp0W6')
		json_data = json.loads(r.text)
		for eachWork in json_data["projects"]:
			work = Works()
			work.title = eachWork["name"]
			work.category = str(eachWork["fields"])[1:-1].replace("'","")
			work.desc = ""
			work.tumbn = eachWork["covers"]["404"]
			work.link = eachWork["url"]
			work.b_id = eachWork["id"]
			work.save()
		return '200'
