from django.db import models

# Create your models here.

class WorkImage(models.Model):
	title = models.CharField(max_length=300)
	picture = models.FileField()

	def __str__(self):
		return self.title

class Works(models.Model):
	
	"""docstring for Works"""
	title = models.CharField(max_length=300)
	category = models.CharField(max_length=150)
	desc = models.TextField()
	tumbn = models.FileField()
	link = models.TextField()
	b_id = models.IntegerField()

	def __str__(self):
		return self.title



		