# Generated by Django 2.1.dev20180405145536 on 2018-04-25 02:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('works', '0002_auto_20180425_0237'),
    ]

    operations = [
        migrations.AddField(
            model_name='works',
            name='picture',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='works.WorkImage'),
            preserve_default=False,
        ),
    ]
