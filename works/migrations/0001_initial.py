# Generated by Django 2.1.dev20180405145536 on 2018-04-24 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Works',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=300)),
                ('category', models.CharField(max_length=150)),
                ('desc', models.TextField()),
                ('tumbn', models.FileField(upload_to='')),
                ('picture', models.FileField(upload_to='')),
            ],
        ),
    ]
