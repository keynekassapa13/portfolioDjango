(function() {
	fadeOut();

	$('.col-sm-4').click(function(e) {
		e.preventDefault();
		var element = $(this);
		var href = element.find('a').attr('href');
		redirectTo(href);

	});
})(jQuery);