$(document).ready(function() {
	$(document).delegate('.open', 'click', function(event){
		$(this).addClass('oppenned');
		event.stopPropagation();
	})
	$(document).delegate('body', 'click', function(event) {
		$('.open').removeClass('oppenned');
	})
	$(document).delegate('.cls', 'click', function(event){
		$('.open').removeClass('oppenned');
		event.stopPropagation();
	});

});

function redirectTo(page){
	var items = Array(
		"Fun Fact bout me: Grew up in Indonesia. Hello, my fellow Indonesian!",
		'Fun Fact bout me: I love photography! ',
		'Fun Fact bout me: Big fan of Harry Potter. Lumos! ',
		'Fun Fact bout me: My favorite food is chocolate milkshake. Wait, is it food? ',
		'Fun Fact bout me: Lany is still in my repeated songs ',
		"Fun Fact bout me: Believe me, I can't speak chinese... "
		);
	var item = items[Math.floor(Math.random()*items.length)];

	var change = $('#mainlayer2 h4');
	change.text(item);

	var mainContainer = $('#mainlayer'),
    mainContainer2 = $('#mainlayer2'),
    discoverMore = $('.container').find('.cta'),
    textContainer = $('#mainlayer2 h4');

	mainContainer.css('display','block');
	mainContainer2.css('display','block');
	mainContainer2.css('position','absolute');
	TweenLite.fromTo(mainContainer,0.5,{opacity:1,scale:0,borderRadius:"100%"},{scale:1, borderRadius:"0%", ease: Circ.easeOut});
	TweenLite.fromTo(textContainer,0.5,{scale:0},{scale:1});
	setTimeout(function() {
		location.href = page;
	}, 2000);
};

function fadeOut(){
	var mainContainer = $('#mainlayer');
	mainContainer.css('display','block');
	setTimeout(function() {
		TweenLite.fromTo(mainContainer,0.5,{opacity:1, ease: Circ.easeOut}, {opacity:0});
    	}, 500);

	setTimeout(function() {
		mainContainer.css('display','none');
    	}, 1000);
};