(function() {
	fadeOut();
	var getBothSVG = $('.container');
		  getBothSVG.removeClass('hide-content');
	var tl = new TimelineMax({delay:0.5});
	  var $f1 = $('.layer1 .cls-1'),
	  $f2 = $('.layer1 .cls-2'),
	  $f3 = $('.layer1 .FrontEndBox'),
	  $f4 = $('.layer1 .FrontEndCircle'),
	  $U1 = $('.layer2 .UIUXLay'),
	  $U2 = $('.layer2 .cls-1'),
	  $U3 = $('.layer2 .cls-2'),
	  $U4 = $('.layer2 .UIUXPen'),

	  $B1 = $('.box-text-1'),
	  $B2 = $('.box-text-2')
	  ;


	  tl

	  .fromTo($U4, 0.2, {scale:0, y:400}, {scale:1, y:0, delay:0.4})
	  .staggerFromTo($U1, 0.4, {opacity:0, y:-100}, {opacity:1, y:0}, 0.1)
	  .fromTo($U2, 1, {opacity:0, y:100}, {opacity:1, scale:1, delay:0.3, y:0, ease: Elastic.easeOut.config(1, 0.3)})
	  .staggerFromTo($U3, 0.05, {opacity:0}, {opacity:1}, 0.01)
	  .staggerFromTo($B1, 0.8, {opacity:0, y:50}, {opacity:1, y:0}, 0.2)

	  .fromTo($f3, 0.2, {scale:0}, {scale:1, delay:0.4})
	  .staggerFromTo($f4, 0.2, {scale:0}, {scale:1}, 0.1)
	  .fromTo($f1, 1, {opacity:0, y:100}, {opacity:1, scale:1, delay:0.3, y:0, ease: Elastic.easeOut.config(1, 0.3)})
	  .staggerFromTo($f2, 0.05, {opacity:0}, {opacity:1}, 0.01)
	  .staggerFromTo($B2, 0.8, {opacity:0, y:50}, {opacity:1, y:0}, 0.2)
  ;

  var mainContainer = $('#mainlayer'),
    mainContainer2 = $('#mainlayer2'),
    discoverMore = $('.container').find('.cta'),
    textContainer = $('#mainlayer2 h4');

    discoverMore.on('click',function(e) {
    	mainContainer.css('display','block');
    	mainContainer2.css('display','block');
    	mainContainer2.css('position','absolute');
    	setTimeout(function() {
    		redirectTo('../about/');
    	}, 500);


    });


})(jQuery);