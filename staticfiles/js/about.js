(function() {

	fadeOut();
	var tl = new TimelineMax({delay:1});
	var $A1 = $('.works-1'),
	$L1 = $('.wl-1'),
	$A2 = $('.works-2'),
	$L2 = $('.wl-2'),
	$A3 = $('.works-3'),
	$S1 = $('.skills-1'),
	$L3 = $('.sl-1'),
	$S2 = $('.skills-2'),

	$Wtxt = $('.works-txt'),
	$Stxt = $('.skills-txt')
	;

	tl
	.fromTo($Wtxt, 0.4, {opacity:0, x:-100}, {opacity:1, x:0})
	.staggerFromTo($A1, 0.4, {opacity:0}, {opacity:1}, 0.5)
	.fromTo($L1, 0.4, {scale:0}, {scale:1, delay: 0.2})
	.staggerFromTo($A2, 0.4, {opacity:0}, {opacity:1}, 0.5)
	.fromTo($L2, 0.4, {scale:0}, {scale:1, delay: 0.2})
	.staggerFromTo($A3, 0.4, {opacity:0}, {opacity:1}, 0.5)

	.fromTo($Stxt, 0.4, {opacity:0, x:-100}, {opacity:1, x:0})
	.staggerFromTo($S1, 0.4, {opacity:0}, {opacity:1}, 0.5)
	.fromTo($L3, 0.4, {scale:0}, {scale:1, delay: 0.2})
	.staggerFromTo($S2, 0.4, {opacity:0}, {opacity:1}, 0.5)
	;
})(jQuery);

