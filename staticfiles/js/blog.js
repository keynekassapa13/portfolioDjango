(function() {

	fadeOut();
	var tl = new TimelineMax({delay:1});
	var $B1 = $('.blog-box')
	;

	tl
	.staggerFromTo($B1, 0.3, {opacity:0, y:100, scale:0.5}, {opacity:1, y:0, scale:1, ease: Power2.easeOut}, 0.2)
	;
})(jQuery);

